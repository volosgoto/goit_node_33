router.patch(
    '/avatars',
    auth,
    upload.single('avatar'),
    ctrlWrapper(usersController.updateAvatar)
);

router.patch('/:userId', ctrlWrapper(usersController.updateUserSubscription));
