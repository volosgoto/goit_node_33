class User {
    constructor(name, age, btn) {
        this.name = name;
        this.age = age;
        this.button = btn;
    }

    // Puclic class field
    status = "Admin";
    arr = [1, 2, 3, 4];
    adress = { city: "Kyiv" };

    getInfo = () => {
        console.log(this);
        // console.log(`${this.name} ${this.age}`);
    };

    // getInfo() {
    //     console.log(`${this.name} ${this.age}`);
    // }

    onButtonClick(event) {
        console.log(event.target);
    }

    // addListenets() {
    //     this.button.addEventListener("click", this.onButtonClick.bind(this));
    // }

    addListenets = () => {
        this.button.addEventListener("click", this.onButtonClick);
    };

    init() {
        // console.log("Init");
        this.addListenets();
    }
}

let btn = document.querySelector("button");
// console.log(btn);

let vova = new User("Vava", 25, btn);
vova.init();
// let sara = new User("Sara", 30, btn);

console.log(vova);
// vova.getInfo();
// console.log(vova);
// vova.getInfo();
// console.log(vova.status);

// function sum(df, asfa, fadfsdf){}
