// Spred operator

// Rest     Spred
//   ...  = ...

// let citiesOtUkraine = ["Kyiv", "Liviv", "Odessa", "Donetsk", "Kharkiv"];
// let citiesOtEurope = ["London", "Paris", "Milan", "Berlin"];
// let cities = [...citiesOtUkraine, "New York", ...citiesOtEurope];
// console.log(cities);

// let [AA, TC, BH, AH] = citiesOtUkraine;
// let [, TC, ...rest] = citiesOtUkraine;

// console.log(AA, BH);
// console.log(TC);
// console.log(rest);

let vova = {
    name: "Vova",
    age: 25,
    status: "Admin",
};

// Spred
// let sara = {
//     ...vova,
//     name: "Sara",
//     status: "Moderator",
// };

// console.log(sara);

let { name: userName, ...pizza } = vova;
// console.log(name);
console.log(userName);
// console.log(pizza);
